export PATH=${PWD}/bin:$PATH

pushd artifacts/channel/
. ./create-artifacts.sh
popd

docker-compose -f ./artifacts/docker-compose.yaml down
sleep 5

docker-compose -f ./artifacts/docker-compose.yaml up -d

sleep 5
./createChannel.sh

sleep 2

./deployChaincode.sh